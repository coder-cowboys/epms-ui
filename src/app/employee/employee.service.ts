import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Employee } from './employee';



@Injectable({ providedIn: 'root' })
export class EmployeeService {

  private baseUrl = 'http://localhost:8080';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  addEmployee(employee: Employee): Observable<Employee> {
    return this.http.post<Employee>(this.baseUrl + '/employee', employee, this.httpOptions).pipe(
      tap((newEmployee: Employee) => console.log(`Added employee with id=${newEmployee.employeeId}`))
    );
  }

    /** GET heroes from the server */
    getEmployees(): Observable<Employee[]> {
      return this.http.get<Employee[]>(this.baseUrl + '/employees')
        .pipe(
          tap(_ => console.log('fetched employees'))
        );
    }

}


