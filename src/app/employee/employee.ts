export class Employee {

  constructor(
    public employeeId: number,
    public employeeName: string,
    public artUnit: string,
  ) {  }

}
