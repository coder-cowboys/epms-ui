import { Component,OnInit } from '@angular/core';
import { Employee } from './employee'
import { EmployeeService } from './employee.service'

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})

export class EmployeeComponent  implements OnInit{

  employeeData: any = [];
  employees: Employee[] = [];

  ngOnInit(): void {
    this.initializeEmployee();
    this.getEmployees();
  }

  getEmployees() {
    this.employeeService.getEmployees()
      .subscribe(employees => this.employees = employees);
  }

  initializeEmployee() {
    this.employeeData = new Employee(0,'','');
  }

  constructor(private employeeService: EmployeeService) { }

  onSubmit() {

    this.employeeService.addEmployee(this.employeeData)
      .subscribe(
        (response: any) => {
          console.log('Employee added successfully!');
          this.initializeEmployee();
          this.getEmployees();
        }
      );
    }

}
